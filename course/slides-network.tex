\documentclass{beamer}

\useoutertheme{infolines}
\usecolortheme{rose} %background of blocks
\usetheme{NathCours}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{url}
\usepackage{wasysym}
\newcommand{\email}[1]{\href{mailto:#1}{\nolinkurl{#1}}}
\usepackage{multimedia} 
\usepackage{algorithm,algorithmic}
\usepackage{listings}
\lstloadlanguages{R}

\definecolor{vi3}{rgb}{0.5,0,0.5}
\definecolor{jaunepale}{cmyk}{0,0,0.2,0}
\definecolor{tu3}{rgb}{0,0.5,0}
\definecolor{brik}{rgb}{0.8,0.2,0.2}
\definecolor{frenchcol}{rgb}{0.245,0.118,0.45}
\definecolor{darkred}{rgb}{0.7,0,0}
\definecolor{vert}{rgb}{0,1,0}
\definecolor{vertclair}{RGB}{123,184,165}
\definecolor{roseclair}{RGB}{214,126,174}

\newcommand{\french}[1]{\textcolor{frenchcol}{\it #1}}
\newcommand{\citer}[1]{\textcolor{darkred}{\bf \cite{#1}}}

\hypersetup{colorlinks=true,
citecolor=vi3, %
pdfmenubar=false,
pdftoolbar=false,
bookmarksopen=true,
breaklinks=true, %permet le retour à la ligne dans les liens trop longs
urlcolor=vi3, %couleur des hyperliens mais la couleur ne change pas !!!
linkcolor=vi3}

% From Philippe Besse for Rcode with listings
\lstnewenvironment{Rcode}{\lstset{language=R,basicstyle=\ttfamily,
keywordstyle=\color{vi3},commentstyle=\small\color{tu3},backgroundcolor=\color{
jaunepale},xleftmargin=2ex,showstringspaces=false}}{}

\def\RR{\textsf{R}\/}
\newcommand{\strong}[1]{{\normalfont\fontseries{b}\selectfont #1}}
\let\pkg=\strong
\RequirePackage{alltt}

\DeclareGraphicsExtensions{.pdf,.jpg,.png}
\graphicspath{{../images/}{images/}}

\begin{document}
\sloppy
\selectlanguage{english}

\logo{\includegraphics[width=2 cm]{inra.jpg}}

\title[Network]{An introduction to network inference and mining}
\author[Formation INRA]{Nathalie Villa-Vialaneix -
\email{nathalie.villa@toulouse.inra.fr}\\ {\url{http://www.nathalievilla.org}}}
\institute[Niveau 3]{INRA, UR 875 MIAT}

\date[Nathalie Villa-Vialaneix]{Formation Biostatistique, Niveau 3}

\begin{frame}[label=slide1]
\titlepage
\end{frame}

\mode<presentation>{\begin{frame}[label=slide2]
	\frametitle{Outline}
	\tableofcontents
\end{frame}}


\AtBeginSection[]{
\begin{frame}
\frametitle{Outline}
\tableofcontents[current]
\end{frame}}

\mode<article>{\maketitle}

\section{A brief introduction to networks/graphs}

\begin{frame}
	\frametitle{What is a network/graph? \french{réseau/graphe}}
	Mathematical object used to model \valeur{relational data between entities}.
	\vspace*{0.5 cm}
	
	\onslide*<2|handout:1>{The entities are called the \valeur{nodes} or the
\valeur{vertexes} (vertices in British)\\
	\french{n\oe uds/sommets}\\
	\begin{pgfpicture}{0 cm}{0 cm}{10 cm}{6 cm}
		\pgfsetfillcolor{frenchcol}
		\pgfcircle[fill]{\pgfxy(2,2)}{0.5 cm}
		\pgfcircle[fill]{\pgfxy(0.5,0.5)}{0.5 cm}
		\pgfcircle[fill]{\pgfxy(5,5)}{0.5 cm}
		\pgfcircle[fill]{\pgfxy(9,5)}{0.5 cm}
		\pgfcircle[fill]{\pgfxy(9,1)}{0.5 cm}
	\end{pgfpicture}
	}
	\onslide+<3|handout:2>{A relation between two entities is modeled by an
\valeur{edge}\\
	\french{arête}\\
	\begin{pgfpicture}{0 cm}{0 cm}{10 cm}{6 cm}
		\pgfsetfillcolor{frenchcol}
		\pgfcircle[fill]{\pgfxy(2,2)}{0.5 cm}
		\pgfcircle[fill]{\pgfxy(0.5,0.5)}{0.5 cm}
		\pgfcircle[fill]{\pgfxy(5,5)}{0.5 cm}
		\pgfcircle[fill]{\pgfxy(9,5)}{0.5 cm}
		\pgfcircle[fill]{\pgfxy(9,1)}{0.5 cm}
		\pgfsetlinewidth{1.2 pt}
		\pgfxyline(0.85,0.85)(1.65,1.65)
		\pgfxyline(1,0.5)(8.5,1)
		\pgfxyline(1,0.5)(8.65,4.65)
		\pgfxyline(9,1.5)(9,4.5)
		\pgfxyline(8.65,1.35)(5.35,4.65)
		\pgfxyline(8.5,5)(5.5,5)
		\pgfxyline(4.65,4.65)(2.35,2.35)
		\pgfxyline(2.35,1.65)(8.5,1)
	\end{pgfpicture}
	\mode<presentation>{\vspace*{2 cm}}
	}
\end{frame}

\begin{frame}
	\frametitle{(non biological) Examples}
	\onslide*<1|handout:1>{\valeur{Social network}: nodes: persons - edges: 2
persons are connected (``friends'')
	\begin{center}
		\includegraphics[width=8 cm]{my-fb-network-fr.png} {\tiny (Natty's
facebook$^{\texttrademark}$\footnote{\tiny \url{https://www.facebook.com}}
network)}
	\end{center}
	}
	\onslide*<2-3|handout:2-3>{\begin{block}{Modeling a large corpus of medieval
documents}
		\onslide*<2|handout:2>{\begin{tabular}{ll}
			\includegraphics[width=3 cm]{bailafief.jpeg} & \begin{minipage}{7
cm}
				Notarial acts (mostly \french{baux à fief}, more precisely, land
charters) established in a \french{seigneurie} named ``Castelnau Montratier'',
written between 1250 and 1500, involving tenants and lords.\footnote{\tiny
\url{http://graphcomp.univ-tlse2.fr}}
			\end{minipage}
		\end{tabular}}
		\onslide*<3|handout:3>{\begin{tabular}{ll}
			\includegraphics[width=3.5 cm]{medieval-zoom.png} &
\begin{minipage}{7 cm}
				\begin{itemize}
					\item nodes: transactions and individuals (3~918 nodes)
					\item edges: an individual is directly involved in a transaction
(6~455 edges)
				\end{itemize}
			\end{minipage}
		\end{tabular}}
	\end{block}}
	\onslide<4|handout:0>{
	\begin{center}
		\includegraphics[height=7 cm]{medieval.png}
	\end{center}}
\end{frame}

\begin{frame}
	\frametitle{Standard issues associated with networks}
	\begin{block}{Inference}
		Giving data, how to build a graph whose edges represent the \valeur{direct}
links between variables?\\
		\onslide*<1>{\footnotesize \valeur{Example}: co-expression networks built
from microarray data (nodes = genes; edges = significant ``direct links''
between expressions of two genes)}
	\end{block}
	\pause

	\begin{block}{Graph mining (examples)}
		\begin{enumerate}
			\item \valeur{Network visualization}: nodes \valeur{are not} a priori
associated to a given position. How to represent the network in a meaningful
way?
			\onslide*<2|handout:1>{
			\begin{tabular}{cc}
				\includegraphics[width=2.5 cm]{my-fb-network-raw.png} &
\includegraphics[width=2.5 cm]{my-fb-network-fr.png}\\
				Random positions & \begin{minipage}{5 cm}
					Positions aiming at representing connected nodes closer
				\end{minipage}
			\end{tabular}}
			\item<3-|handout:2> \valeur{Network clustering}: identify ``communities''
{\footnotesize (groups of nodes that are densely connected and share a few links
(comparatively) with the other groups)}
			\begin{center}
				\includegraphics[width=4 cm]{my-fb-network-communities.png}
			\end{center}
		\end{enumerate}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{More complex relational models}
	Nodes may be \valeur{labeled} by a factor
	\onslide*<1|handout:1>{\begin{center}
		\includegraphics[width=3.5 cm]{medieval-zoom.png}
	\end{center}}
	\onslide<2-|handout:2>{
		\begin{center}
		\includegraphics[width=8 cm]{bionetwork-ph.png}
	\end{center}
	... or by a numerical information. \citer{laurent_villavialaneix_III2011}}

	\onslide<3-|handout:2>{
	\valeur{Edges} may also be labeled (type of the relation) or weighted
(strength of the relation) or directed (direction of the relation).
	\mode<presentation>{\vspace*{2 cm}}}
\end{frame}


\section{Network inference}

\begin{frame}
	\frametitle{Framework}
	\valeur{Data}: large scale gene expression data
	\[
		\begin{array}{l}
			\textrm{individuals}\\
			n \simeq 30/50
		\end{array}
		\underbrace{\left\{X=\left(\begin{array}{cccccc}
		. & . & . & . & . & .\\
		. & . & X_i^j & . & . & .\\
		. & . & . & . & . & .
		\end{array}\right)\right.}_{\textrm{variables (genes expression)},\ p\simeq
10^{3/4}}
	\]
	\valeur{What we want to obtain}: a network with
	\begin{itemize}
		\item nodes: genes;
		\item edges: significant and direct co-expression between two genes (track
transcription regulations)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Advantages of inferring a network from large scale transcription
data}
	\begin{enumerate}
		\item \valeur{over raw data}: \valeur{focuses on the strongest direct
relationships}: irrelevant
or indirect relations are removed (more robust) and the data are easier to
visualize and
understand.\\
Expression data are \valeur{analyzed all together} and not by pairs.\\
\vspace*{1 cm}
		\item<2-> \valeur{over bibliographic network}: can handle
\valeur{interactions with yet unknown} (not annotated) \valeur{genes} and deal
with data collected in a particular condition.
	\end{enumerate}
\end{frame}

\begin{frame}
	\frametitle{Using \emph{correlations}: relevance
network \citer{butte_kohane_AMIAS1999,butte_kohane_PSB2000}}
	\valeur{First (naive) approach}: calculate correlations between expressions
for all pairs of genes, threshold the smallest ones and build the network.
  	\begin{columns}[t]
		\begin{column}[c]{0.3\linewidth}
		
\includegraphics[width=\linewidth]{heatmap-nv2.png}\\
			``Correlations''
		\end{column}
		\begin{column}[c]{0.3\linewidth}
		
\includegraphics[width=\linewidth]{thres-heatmap-nv2.png}\\
			Thresholding
		\end{column}
				\begin{column}[c]{0.3\linewidth}
		
\includegraphics[width=\linewidth]{thegraph-nv2.png}\\
			Graph
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{But correlation is not causality...}
	\onslide*<1>{
	\includegraphics[width=\linewidth]{pluie.jpg}
	}
	\onslide<2->{\begin{center}
		\begin{pgfpicture}{-0.5 cm}{-0.5 cm}{6.5 cm}{4 cm}
			\color{darkred}
			\pgfsetlinewidth{2 pt}
			\pgfsetendarrow{\pgfarrowtriangle{4 pt}}
			\pgfxyline(3,3)(0.45,0.45)
			\pgfxyline(3,3)(5.55,0.45)
			\color{purple}
			\pgfputat{\pgfxy(3,-0.1)}{\pgfbox[center,top]{strong indirect
correlation}}
			\pgfclearendarrow
			\pgfsetlinewidth{1.5 pt}
			\pgfsetdash{{0.3 cm}{0.2 cm}}{0 cm}
			\pgfxyline(0,0)(6,0)
			\pgfsetfillcolor{roseclair}
			\pgfcircle[fill]{\pgfxy(0,0)}{0.5 cm}
			\pgfcircle[fill]{\pgfxy(6,0)}{0.5 cm}
			\pgfcircle[fill]{\pgfxy(3,3)}{0.5 cm}
			\color{black}
			\pgfputat{\pgfxy(0,0)}{\pgfbox[center,center]{$y$}}
			\pgfputat{\pgfxy(6,0)}{\pgfbox[center,center]{$z$}}
			\pgfputat{\pgfxy(3,3)}{\pgfbox[center,center]{$x$}}
		\end{pgfpicture}
	\end{center}}
	\onslide*<2-3>{\fbox{\begin{minipage}{10 cm}
		{\tt \footnotesize set.seed(2807); x <- runif(100)\\
		y <- 2*x+1+rnorm(100,0,0.1); cor(x,y); \textcolor{darkred}{[1] 0.9988261}\\
		z <- 2*x+1+rnorm(100,0,0.1); cor(x,z); \textcolor{darkred}{[1] 0.998751}\\
		cor(y,z); \textcolor{darkred}{[1] 0.9971105}\\
		\onslide+<3>{\textcolor{vertclair}{$\sharp$ Partial correlation}\\
		cor(lm(y$\sim$x)\$residuals,lm(z$\sim$x)\$residuals)
\textcolor{darkred}{[1] -0.1933699}}}
	\end{minipage}}}
	\onslide<4>{Networks are built using \valeur{partial correlations}, i.e.,
correlations between gene expressions \valeur{knowing the expression of all the
other genes} (residual correlations).\vspace*{2 cm}}
\end{frame}

\begin{frame}
	\frametitle{Various approaches (and \includegraphics[width=0.5
cm]{R.jpg}\ packages) to infer gene expression networks}
	\begin{itemize}
		\item \valeur{Graphical Gaussian Model}
		\onslide*<1-2>{
		$(X_i)_{i=1,\ldots,n}$ are i.i.d. Gaussian random variables
$\mathcal{N}(0,\Sigma)$ (gene expression); then
		\[
			j \longleftrightarrow j' \textrm{(genes }j\textrm{ and }j'\textrm{ are
linked)} \Leftrightarrow \mathbb{C}\textrm{or}\left(X^j,X^{j'}|(X^k)_{k\neq
j,j'}\right)>0 
		\]
		\valeur{$\mathbb{C}\textrm{or}\left(X^j,X^{j'}|(X^k)_{k\neq j,j'}\right)
\simeq \left(\Sigma^{-1}\right)_{j,j'} \Rightarrow$} find the partial
correlations by means of $(\widehat{\Sigma}^n)^{-1}$.

		\onslide*<2>{\valeur{Problem:} $\Sigma$ is a $p$-dimensional matrix (with
$p$
large) and $n$ is small compared to $p$ $\Rightarrow$
$(\widehat{\Sigma}^n)^{-1}$ is a poor estimate of $\Sigma^{-1}$!}}

		\onslide<3->{\begin{itemize}
			\item<3-> seminal work:
\citer{schafer_strimmer_B2005,schafer_strimmer_SAGMB2005} (with bootstrapping or
shrinkage and a proposal for a Bayesian test for significance); package
\pkg{GeneNet};
			\item<4-> sparse approaches \citer{friedman_etal_B2008}: packages
\pkg{glasso}, \pkg{huge}, \pkg{GGMselect} \citer{giraud_etal_p2009},
\pkg{SIMoNe} \citer{chiquet_etal_B2009}, \pkg{JGL}
\citer{danaher_etal_JRSSB2014} or \pkg{therese}
\citer{villavialaneix_etal_QTQM2014}... (with unsupervised
clustering or able to handle multiple populations data)
		\end{itemize}}
		\item<5-> \valeur{Other methods}: Bayesian network learning
\citer{pearl_RRS1998,pearl_russel_HBTNN2002,scutari_JSS2010} \pkg{bnlearn},
mutual information \citer{meyer_etal_BMCB2008} \pkg{minet}... \vspace*{3 cm}
	\end{itemize}
\end{frame}

\section{Simple graph mining}

\begin{frame}
	\frametitle{Settings}
	\begin{block}{Notations}
		In the following, a \valeur{graph} $\mathcal{G}=(V,E,W)$ with:
		\begin{itemize}
			\item $V$: set of vertexes $\{x_1,\ldots,x_p\}$;
			\item $E$: set of edges;
			\item $W$: weights on edges s.t. $W_{ij}\geq 0$, $W_{ij}=W_{ji}$ and
$W_{ii}=0$.
		\end{itemize}
	\end{block}

	\onslide*<2-3|handout:1>{The graph is said to be
\valeur{connected}/\french{connexe} if any node can be reached from any other
node by a path/\french{un chemin}.}

	\onslide*<3|handout:1>{The \valeur{connected components}/\french{composantes
connexes} of a graph are all its connected subgraphs.}
	\onslide*<4|handout:2>{\valeur{Example 1}: Natty's FB network has 21 connected
components with 122 vertexes (professional contacts, family and closest
friends) or from 1 to 5 vertexes (isolated nodes)
	\begin{center}
		\includegraphics[width=6 cm]{my-fb-network-fr-total.png}
	\end{center}}
	\onslide<5|handout:3>{\valeur{Example 2}: Medieval network: 10~542 nodes and
the largest connected component contains 10~025 nodes (``giant component'' /
\french{composante géante}).\vspace*{5 cm}}
\end{frame}


\subsection{Visualization}

\begin{frame}
	\frametitle{Visualization tools help understand the graph macro-structure}
	\valeur{Purpose}: How to display the nodes in a \valeur{meaningful} and
\valeur{aesthetic} way?
	\pause

	Standard approach: \valeur{force directed placement} algorithms (FDP)
\french{algorithmes de forces} (e.g., \citer{fruchterman_reingold_SPE1991})
	\begin{center}
		\begin{pgfpicture}{0 cm}{0 cm}{5 cm}{3 cm}
			\color{purple}
			\pgfsetlinewidth{1.5 pt}
			\pgfxyline(1,1)(3,2.5)
			\pgfxyline(1,1)(4,1.5)
			\color{pink}
			\pgfcircle[fill]{\pgfxy(1,1)}{0.3 cm}
			\pgfcircle[fill]{\pgfxy(3,2.5)}{0.3 cm}
			\pgfcircle[fill]{\pgfxy(4,1.5)}{0.3 cm}
			\color{purple}
			\pgfsetlinewidth{1.2 pt}
			\pgfcircle[stroke]{\pgfxy(1,1)}{0.3 cm}
			\pgfcircle[stroke]{\pgfxy(3,2.5)}{0.3 cm}
			\pgfcircle[stroke]{\pgfxy(4,1.5)}{0.3 cm}
			\onslide*<3>{\color{blue}
			\pgfsetlinewidth{0.5 pt}
			\pgfsetendarrow{\pgfarrowswap{\pgfarrowtriangle{2 pt}}}
			\pgfsetstartarrow{\pgfarrowswap{\pgfarrowtriangle{2 pt}}}
			\pgfxyline(1,1)(3,2.5)
			\pgfxyline(1,1)(4,1.5)}
			\onslide*<4>{\color{red}
			\pgfsetlinewidth{0.5 pt}
			\pgfsetendarrow{{\pgfarrowtriangle{2 pt}}}
			\pgfsetstartarrow{{\pgfarrowtriangle{2 pt}}}
			\pgfxyline(1,1)(3,2.5)
			\pgfxyline(1,1)(4,1.5)
			\pgfxyline(4,1.5)(3,2.5)}
		\end{pgfpicture}
	\end{center}
	\begin{itemize}
		\item<3-> \textcolor{blue}{attractive forces}: similar to springs along the
edges
		\item<4-> \textcolor{red}{repulsive forces}: similar to electric forces
between all pairs of vertexes
	\end{itemize}
	\onslide<5->{\valeur{iterative} algorithm until stabilization of the vertex
positions.}
\end{frame}

\begin{frame}
	\frametitle{Visualization software}
	\begin{itemize}
		\item \includegraphics[width=0.5 cm]{R.jpg}\ package
\texttt{igraph}\footnote{\tiny \url{http://igraph.sourceforge.net/}}
\citer{csardi_nepusz_I2006} (static representation with useful tools for graph
mining) 
		\item<2-> \includegraphics[width=1 cm]{gephi.jpg} free software
\valeur{Gephi}\footnote{\tiny \url{http://gephi.org}} (interactive software,
supports zooming and panning)
		\onslide<2->{\begin{center}
			\movie[label=cells,width=7 cm,height=4.5
cm,poster,showcontrols,duration=5s]{}{videos/ex-gephi.mov}
		\end{center}}
	\end{itemize}
\end{frame}


\subsection{Global characteristics}

\begin{frame}
	\frametitle{Peculiar graphs}
	Medieval network (largest connected component):
	\begin{itemize}
		\item 10~025 vertexes: transactions or persons;
		\item edges model the active involvement of a person in a transaction.
	\end{itemize}
	\valeur{$\Rightarrow$ \valeur{Bipartite} graph / \french{graphe biparti}}
	\onslide*<1|handout:1>{\begin{center}
		\begin{pgfpicture}{0 cm}{0 cm}{6 cm}{3.5 cm}
			\pgfsetlinewidth{1.2 pt}
			\pgfxyline(0,0)(6,1)
			\pgfxyline(0,0)(6,2.5)
			\pgfxyline(0,1.5)(6,2.5)
			\pgfxyline(0,3)(6,1)
			\pgfsetfillcolor{frenchcol}
			\pgfcircle[fill]{\pgfxy(0,0)}{0.3 cm}
			\pgfcircle[fill]{\pgfxy(0,1.5)}{0.3 cm}
			\pgfcircle[fill]{\pgfxy(0,3)}{0.3 cm}
			\pgfsetfillcolor{orange}
			\pgfcircle[fill]{\pgfxy(6,1)}{0.3 cm}
			\pgfcircle[fill]{\pgfxy(6,2.5)}{0.3 cm}
	\end{pgfpicture}
	\end{center}}

	\onslide<2-|handout:2>{\valeur{Projected graphs}:
	\begin{itemize}
		\item individuals: nodes are the 3~755 individuals and edges weighted by the
number of common transactions;
		\item transactions (not used): nodes are the 6~270 transactions and edges
are weighted by the number of common actively involved persons.
	\end{itemize}}
	\onslide*<2>{\begin{center}
		\begin{pgfpicture}{0 cm}{0 cm}{6 cm}{2 cm}
			\pgfsetlinewidth{1.2 pt}
			\pgfxyline(0,0)(3,0.5)
			\pgfxyline(0,0)(3,1.25)
			\pgfxyline(0,0.75)(3,1.25)
			\pgfxyline(0,1.5)(3,0.5)
			\pgfxyline(4.5,0)(6,1)
			\pgfxyline(4.5,2)(4.5,0)
			\pgfsetfillcolor{frenchcol}
			\pgfcircle[fill]{\pgfxy(0,0)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(0,0.75)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(0,1.5)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(4.5,0)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(6,1)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(4.5,2)}{0.2 cm}
			\pgfsetfillcolor{orange}
			\pgfcircle[fill]{\pgfxy(3,0.5)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(3,1.25)}{0.2 cm}
			\color{black}
			\pgfputat{\pgfxy(0,0)}{\pgfbox[center,center]{1}}
			\pgfputat{\pgfxy(0,0.75)}{\pgfbox[center,center]{2}}
			\pgfputat{\pgfxy(0,1.5)}{\pgfbox[center,center]{3}}
			\pgfputat{\pgfxy(4.5,0)}{\pgfbox[center,center]{1}}
			\pgfputat{\pgfxy(6,1)}{\pgfbox[center,center]{2}}
			\pgfputat{\pgfxy(4.5,2)}{\pgfbox[center,center]{3}}
			\pgfputat{\pgfxy(4.4,1)}{\pgfbox[center,right]{1}}
			\pgfputat{\pgfxy(5,0.5)}{\pgfbox[center,left]{1}}
	\end{pgfpicture}
	\end{center}}
\end{frame}

\begin{frame}
	\frametitle{Density / Transitivity \french{Densité / Transitivité}}
	\valeur{Density}: Number of edges divided by the number of pairs of vertexes.
{\it Is the network densely connected?}
	\onslide*<2|handout:1>{\begin{block}{Examples}
		\valeur{Example 1}: Natty's FB network
		\begin{itemize}
			\item 152 vertexes, 551 edges $\Rightarrow$ density $=\frac{551}{152\times
151/2}\simeq 4.8$\%;
			\item largest connected component: 122 vertexes, 535 edges $\Rightarrow$
density $\simeq 7.2$\%.
		\end{itemize}
		\valeur{Example 2}: Medieval network (largest connected component): 10~025
vertexes, 17~612 edges $\Rightarrow$ density $\simeq 0.035$\%.\\
		Projected network (individuals): 3~755 vertexes, 8~315 edges $\Rightarrow$
density $\simeq 0.12$\%.
	\end{block}}

	\onslide<3-|handout:2->{\valeur{Transitivity}: Number of triangles divided by
the number of triplets connected by at least two edges. {\it What is the
probability that two friends of mine are also friends?}}
	\onslide*<3|handout:2>{
	\begin{center}
		\begin{pgfpicture}{4 cm}{0 cm}{10 cm}{3 cm}
			\pgfsetlinewidth{1.2 pt}
			\pgfxyline(4,1)(7,2)
			\pgfxyline(7,2)(7,0)
			\pgfxyline(7,0)(10,0.8)
			\pgfxyline(10,0.8)(7,2)
			\pgfsetfillcolor{frenchcol}
			\pgfcircle[fill]{\pgfxy(7,2)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(7,0)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(10,0.8)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(4,1)}{0.2 cm}
		\end{pgfpicture}
	\end{center}
	Density is equal to $\frac{4}{4\times 3/2}=2/3$ ; Transitivity is equal to
$1/3$.}
	\onslide*<4|handout:3>{
	\begin{center}
		\begin{pgfpicture}{4 cm}{0 cm}{10 cm}{3 cm}
			\pgfsetlinewidth{1.2 pt}
			\pgfxyline(4,1)(7,2)
			\pgfxyline(7,2)(7,0)
			\pgfxyline(7,0)(10,0.8)
			\pgfxyline(10,0.8)(7,2)
			\pgfsetfillcolor{frenchcol}
			\pgfcircle[fill]{\pgfxy(4,1)}{0.2 cm}
			\color{green}
			\pgfcircle[fill]{\pgfxy(7,2)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(7,0)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(10,0.8)}{0.2 cm}
		\end{pgfpicture}
	\end{center}}
	\onslide*<5|handout:4>{
	\begin{center}
		\begin{pgfpicture}{4 cm}{0 cm}{10 cm}{3 cm}
			\pgfsetlinewidth{1.2 pt}
			\pgfxyline(4,1)(7,2)
			\pgfxyline(7,2)(7,0)
			\pgfxyline(7,0)(10,0.8)
			\pgfxyline(10,0.8)(7,2)
			\pgfsetfillcolor{frenchcol}
			\pgfcircle[fill]{\pgfxy(7,0)}{0.2 cm}
			\color{darkred}
			\pgfcircle[fill]{\pgfxy(7,2)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(4,1)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(10,0.8)}{0.2 cm}
		\end{pgfpicture}
	\end{center}}
	\onslide*<6|handout:5>{
	\begin{center}
		\begin{pgfpicture}{4 cm}{0 cm}{10 cm}{3 cm}
			\pgfsetlinewidth{1.2 pt}
			\pgfxyline(4,1)(7,2)
			\pgfxyline(7,2)(7,0)
			\pgfxyline(7,0)(10,0.8)
			\pgfxyline(10,0.8)(7,2)
			\pgfsetfillcolor{frenchcol}
			\pgfcircle[fill]{\pgfxy(10,0.8)}{0.2 cm}
			\color{darkred}
			\pgfcircle[fill]{\pgfxy(7,2)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(4,1)}{0.2 cm}
			\pgfcircle[fill]{\pgfxy(7,0)}{0.2 cm}
		\end{pgfpicture}
	\end{center}}

	\onslide<7|handout:6>{\begin{block}{Examples}
		\valeur{Example 1}: Natty's FB network
		\begin{itemize}
			\item density $\simeq 4.8$\%, transitivity $\simeq 56.2$\%;
			\item largest connected component: density $\simeq 7.2$\%, transitivity
$\simeq 56.0$\%.
		\end{itemize}
		\valeur{Example 2}: Medieval network (projected network, individuals):
density $\simeq 0.12$\%, transitivity $\simeq 6.1\%$.
	\end{block}\vspace*{3 cm}}
\end{frame}


\subsection{Numerical characteristics calculation}

\begin{frame}
	\frametitle{Extracting important nodes}
	\begin{enumerate}
		\item {\footnotesize \valeur{vertex degree} \french{degré}: number of edges
adjacent to a given vertex or $d_i=\sum_j W_{ij}$.} \onslide*<1-3>{Vertexes with
a high degree are called \valeur{hubs}: measure of the vertex popularity.\\
		\onslide*<1|handout:1>{\begin{center}
			\vspace*{-0.5 cm}

			\includegraphics[width=8.5 cm]{my-fb-network-deg-dist.png}\\
			\valeur{Number of nodes ($y$-axis) with a given degree ($x$-axis)}
		\end{center}}
		\onslide*<2|handout:2>{\begin{center} \vspace*{-0.5 cm}
			\includegraphics[width=8 cm]{my-fb-network-fr-hubs.png}\\
			Two hubs are students who have been hold back at school and the other
two are from my most recent class.
		\end{center}}
		\onslide*<3|handout:0>{\includegraphics[width=7.5
cm]{medieval-hubs.png}}}
		\onslide*<4-|handout:3>{The \valeur{degree distribution} is known to fit a
\valeur{power law} \french{loi de puissance} in most real networks:\\
		\onslide*<4|handout:3>{\includegraphics[width=8
cm]{medieval-degreedistribution.pdf}\\
This distribution indicates \valeur{preferential attachement}
\french{attachement préférentiel}.
}}
		\item<5-|handout:4-> {\footnotesize \valeur{vertex betweenness}
\french{centralité}: number of shortest paths between all pairs of vertexes that
pass through the vertex.} Betweenness is a centrality measure {\footnotesize
(vertexes that are likely to disconnect the network if removed)}.\\
		\onslide*<5-9|handout:4-8>{\begin{center}}
			\onslide*<5|handout:4>{\begin{pgfpicture}{0 cm}{0 cm}{10 cm}{3 cm}
				\pgfsetlinewidth{1.2 pt}
				\pgfxyline(0,2)(4,1)
				\pgfxyline(4,1)(7,2)
				\pgfxyline(4,1)(10,0.8)
				\pgfxyline(7,2)(7,0)
				\pgfxyline(7,0)(10,0.8)
				\pgfsetfillcolor{frenchcol}
				\pgfcircle[fill]{\pgfxy(0,2)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(7,2)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(7,0)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(10,0.8)}{0.2 cm}
				\pgfsetfillcolor{orange}
				\pgfcircle[fill]{\pgfxy(4,1)}{0.3 cm}
			\end{pgfpicture}\\
			The orange node's degree is equal to 2, its betweenness to 4.}
			\onslide*<6|handout:5>{\begin{pgfpicture}{0 cm}{0 cm}{10 cm}{3 cm}
				\pgfsetlinewidth{1.2 pt}
				\pgfxyline(4,1)(10,0.8)
				\pgfxyline(7,2)(7,0)
				\pgfxyline(7,0)(10,0.8)
				\color{yellow}
				\pgfxyline(0,2)(4,1)
				\pgfxyline(4,1)(7,2)
				\pgfsetfillcolor{frenchcol}
				\pgfcircle[fill]{\pgfxy(10,0.8)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(7,0)}{0.2 cm}
				\pgfsetfillcolor{yellow}
				\pgfcircle[fill]{\pgfxy(0,2)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(7,2)}{0.2 cm}
				\pgfsetfillcolor{orange}
				\pgfcircle[fill]{\pgfxy(4,1)}{0.3 cm}
			\end{pgfpicture}}
			\onslide*<7|handout:6>{\begin{pgfpicture}{0 cm}{0 cm}{10 cm}{3 cm}
				\pgfsetlinewidth{1.2 pt}
				\pgfxyline(7,2)(7,0)
				\pgfxyline(7,0)(10,0.8)
				\pgfxyline(4,1)(7,2)
				\color{yellow}
				\pgfxyline(0,2)(4,1)
				\pgfxyline(4,1)(10,0.8)
				\pgfsetfillcolor{frenchcol}
				\pgfcircle[fill]{\pgfxy(7,0)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(7,2)}{0.2 cm}
				\pgfsetfillcolor{yellow}
				\pgfcircle[fill]{\pgfxy(0,2)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(10,0.8)}{0.2 cm}
				\pgfsetfillcolor{orange}
				\pgfcircle[fill]{\pgfxy(4,1)}{0.3 cm}
			\end{pgfpicture}}
			\onslide*<8|handout:7>{\begin{pgfpicture}{0 cm}{0 cm}{10 cm}{3 cm}
				\pgfsetlinewidth{1.2 pt}
				\pgfxyline(7,2)(7,0)
				\pgfxyline(4,1)(7,2)
				\color{yellow}
				\pgfxyline(0,2)(4,1)
				\pgfxyline(4,1)(10,0.8)
				\pgfxyline(7,0)(10,0.8)
				\pgfsetfillcolor{frenchcol}
				\pgfcircle[fill]{\pgfxy(7,2)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(10,0.8)}{0.2 cm}
				\pgfsetfillcolor{yellow}
				\pgfcircle[fill]{\pgfxy(0,2)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(7,0)}{0.2 cm}
				\pgfsetfillcolor{orange}
				\pgfcircle[fill]{\pgfxy(4,1)}{0.3 cm}
			\end{pgfpicture}}
			\onslide*<9|handout:8>{\begin{pgfpicture}{0 cm}{0 cm}{10 cm}{3 cm}
				\pgfsetlinewidth{1.2 pt}
				\pgfxyline(4,1)(10,0.8)
				\pgfxyline(7,0)(10,0.8)
				\color{yellow}
				\pgfxyline(0,2)(4,1)
				\pgfxyline(7,2)(7,0)
				\pgfxyline(4,1)(7,2)
				\pgfsetfillcolor{frenchcol}
				\pgfcircle[fill]{\pgfxy(7,2)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(10,0.8)}{0.2 cm}
				\pgfsetfillcolor{yellow}
				\pgfcircle[fill]{\pgfxy(0,2)}{0.2 cm}
				\pgfcircle[fill]{\pgfxy(7,0)}{0.2 cm}
				\pgfsetfillcolor{orange}
				\pgfcircle[fill]{\pgfxy(4,1)}{0.3 cm}
			\end{pgfpicture}}
		\onslide*<5-9|handout:4-8>{\end{center}}
		\onslide*<10|handout:9>{\begin{center}
			\includegraphics[width=7 cm]{my-fb-network-bet-dens.png}
		\end{center}}
		\onslide*<11|handout:10>{\begin{tabular}{cc}
			\includegraphics[width=7 cm]{my-fb-network-fr-highbet.png} &
				\begin{minipage}[b]{4 cm}
					Vertexes with a high betweenness ($> 3~000$) are 2 political figures.
				\end{minipage}
		\end{tabular}}
		\onslide<12|handout:11>{\valeur{Example 2}: In the medieval network: moral
persons such as the ``Chapter of Cahors'' or the ``Church of Flaugnac'' have a
high betweenness despite a low degree.\vspace*{5 cm}}
	\end{enumerate}
\end{frame}


\subsection{Clustering}

\begin{frame}
	\frametitle{Vertex clustering \french{classification}}
	 Cluster vertexes into groups that are \valeur{densely connected} and share
\valeur{a few links} (comparatively) \valeur{with the other groups}. Clusters
are often called \valeur{communities} \french{communautés} (social sciences) or
\valeur{modules} \french{modules} (biology).

	\onslide*<2|handout:1>{\valeur{Example 1}: Natty's facebook network
	\begin{center}\vspace*{-0.5 cm}
		\includegraphics[width=8 cm]{my-fb-network-communities.png}
	\end{center}}

	\onslide*<3|handout:2>{\valeur{Example 2}: medieval network
	\begin{center}
		\includegraphics[width=7 cm]{medieval-clusters.png}
	\end{center}}

	\onslide<4-|handout:3>{\valeur{Several clustering methods}:
	\begin{itemize}
		\item min cut minimization {\footnotesize minimizes the number of edges
between clusters};
		\item spectral clustering \citer{vonluxburg_SC2007} and kernel clustering
{\footnotesize uses eigen-decomposition of the
\valeur{Laplacian}/\french{Laplacien}}
		\[
			L_{ij} = \left\{\begin{array}{ll}
				-w_{ij} & \textrm{ if }i\neq j\\
				d_i & \textrm{ otherwise}
			\end{array} \right.
		\]
		(matrix strongly related to the graph structure);
		\item Generative (Bayesian) models \citer{zanghi_etal_PR2008};
		\item Markov clustering {\footnotesize simulate a flow on the graph};
		\item \valeur{modularity maximization}
		\item ... (clustering jungle... see e.g.,
\citer{fortunato_barthelemy_PNAS2007,schaeffer_CSR2007,
brohee_vanhelden_BMCB2006})
	\end{itemize}}
\end{frame}

\begin{frame}
	\frametitle{Find clusters by modularity optimization \french{modularité}}
	The \valeur{modularity} \citer{newman_girvan_PRE2004} of the partition
$(\mathcal{C}_1,\ldots,\mathcal{C}_K)$ is equal to:
	\[
		\mathcal{Q}(\mathcal{C}_1,\ldots,\mathcal{C}_K)=\frac{1}{2m}\sum_{k=1}^K
\sum_{x_i,x_j\in\mathcal{C}_k}\left(W_{ij}-P_{ij}\right)
	\]
	with $P_{ij}$: weight of a ``null model'' (graph with the same degree
distribution but no preferential attachment):
	\[
		\mathval{P_{ij}=\frac{d_id_j}{2m}}
	\]
	with $d_i=\frac{1}{2}\sum_{j\neq i}W_{ij}$.
\end{frame}

\begin{frame}
	\frametitle{Interpretation}
	A good clustering should \valeur{maximize the modularity}:
	\begin{itemize}
		\item $\mathcal{Q}$ $\nearrow$ when $(x_i,x_j)$ are in the \valeur{same
cluster} and \valeur{$W_{ij}\gg P_{ij}$}
		\item $\mathcal{Q}$ $\searrow$ when $(x_i,x_j)$ are in \valeur{two different
clusters} and \valeur{$W_{ij}\gg P_{ij}$}
		\onslide*<1|handout:1>{($m=20$)\\
		\begin{center}
			\begin{pgfpicture}{0 cm}{0 cm}{10 cm}{2 cm}
				\pgfsetlinewidth{1.2 pt}
				\pgfxyline(1,1)(9,1)
				\pgfsetfillcolor{yellow}
				\pgfcircle[fill]{\pgfxy(1,1)}{0.5 cm}
				\pgfcircle[fill]{\pgfxy(9,1)}{0.5 cm}
				\color{black}
				\pgfputat{\pgfxy(4.5,1.2)}{\pgfbox[center,base]{$P_{ij}=7.5$}}
				\pgfputat{\pgfxy(4.5,0.7)}{\pgfbox[center,top]{$W_{ij}=5\Rightarrow
W_{ij}-P_{ij}=-2.5$}}
				\pgfputat{\pgfxy(1,1)}{\pgfbox[center,center]{$d_i=15$}}
				\pgfputat{\pgfxy(9,1)}{\pgfbox[center,center]{$d_j=20$}}
			\end{pgfpicture}
		\end{center}
		$i$ and $j$ in the same cluster decreases the modularity}
		\onslide*<2|handout:2>{($m=20$)\\
		\begin{center}
			\begin{pgfpicture}{0 cm}{0 cm}{10 cm}{2 cm}
				\pgfsetlinewidth{1.2 pt}
				\pgfxyline(1,1)(9,1)
				\pgfsetfillcolor{yellow}
				\pgfcircle[fill]{\pgfxy(1,1)}{0.5 cm}
				\pgfcircle[fill]{\pgfxy(9,1)}{0.5 cm}
				\color{black}
				\pgfputat{\pgfxy(4.5,1.2)}{\pgfbox[center,base]{$P_{ij}=0.05$}}
				\pgfputat{\pgfxy(4.5,0.7)}{\pgfbox[center,top]{$W_{ij}=5\Rightarrow
W_{ij}-P_{ij}=4.95$}}
				\pgfputat{\pgfxy(1,1)}{\pgfbox[center,center]{$d_i=1$}}
				\pgfputat{\pgfxy(9,1)}{\pgfbox[center,center]{$d_j=2$}}
			\end{pgfpicture}
		\end{center}
		$i$ and $j$ in the same cluster increases the modularity}
		\item<3-|handout:3> Modularity
		\begin{itemize}
			\item \valeur{helps separate hubs} ($\neq$ spectral clustering or min cut
criterion);
			\item is not an increasing function of the number of clusters: useful to
\valeur{choose the relevant number of clusters} (with a grid search: several
values are tested, the clustering with the highest modularity is kept) but
modularity has a \valeur{small resolution default} (see
\citer{fortunato_barthelemy_PNAS2007})
		\end{itemize}
	\end{itemize}
	\onslide<4-|handout:3>{\valeur{Main issue}: Optimization = \valeur{NP-complete
problem} (exhaustive search is not not usable)\\
Different solutions are provided in
\citer{newman_girvan_PRE2004,blondel_etal_JSMTE2008,noack_rotta_SEA2009,
rossi_villavialaneix_JSFdS2011} (among others) and some of them are
implemented in the \RR\ package \pkg{igraph}.\vspace*{5 cm}}
\end{frame}


\begin{frame}
	\frametitle{Open issues with clustering (not addressed)}
	\begin{itemize}
		\item overlapping communities \french{communautés recouvrantes};
		\item hierarchical clustering {\footnotesize
\citer{rossi_villavialaneix_JSFdS2011} provides an approach};
		\item ``organized'' clustering (projection on a small dimensional grid) and
clustering for visualization {\footnotesize
\citer{boulet_etal_N2008,rossi_villavialaneix_N2010,
rossi_villavialaneix_JSFdS2011}};
		\item ...
	\end{itemize}
\end{frame}


{\large \valeur{References}}\\

% {\tiny \bibliographystyle{apalike}
% \bibliography{/home/nathalie/Private/Travail/Modeles/LaTeX/bibliototal}}

{\tiny
\begin{thebibliography}{}

\bibitem[Blondel et~al., 2008]{blondel_etal_JSMTE2008}
Blondel, V., Guillaume, J., Lambiotte, R., and Lefebvre, E. (2008).
\newblock Fast unfolding of communites in large networks.
\newblock {\em Journal of Statistical Mechanics: Theory and Experiment},
  P10008:1742--5468.

\bibitem[Boulet et~al., 2008]{boulet_etal_N2008}
Boulet, R., Jouve, B., Rossi, F., and Villa, N. (2008).
\newblock Batch kernel {SOM} and related {L}aplacian methods for social network
  analysis.
\newblock {\em Neurocomputing}, 71(7-9):1257--1273.

\bibitem[Broh\'ee and van Helden, 2006]{brohee_vanhelden_BMCB2006}
Broh\'ee, S. and van Helden, J. (2006).
\newblock Evaluation of clustering algorithms for protein-protein interaction
  networks.
\newblock {\em BMC Bioinformatics}, 7(488).

\bibitem[Butte and Kohane, 1999]{butte_kohane_AMIAS1999}
Butte, A. and Kohane, I. (1999).
\newblock Unsupervised knowledge discovery in medical databases using relevance
  networks.
\newblock In {\em Proceedings of the AMIA Symposium}, pages 711--715.

\bibitem[Butte and Kohane, 2000]{butte_kohane_PSB2000}
Butte, A. and Kohane, I. (2000).
\newblock Mutual information relevance networks: functional genomic clustering
  using pairwise entropy measurements.
\newblock In {\em Proceedings of the Pacific Symposium on Biocomputing}, pages
  418--429.

\bibitem[Chiquet et~al., 2009]{chiquet_etal_B2009}
Chiquet, J., Smith, A., Grasseau, G., Matias, C., and Ambroise, C. (2009).
\newblock {SIMoNe}: {S}tatistical {I}nference for {MO}dular {NE}tworks.
\newblock {\em Bioinformatics}, 25(3):417--418.

\bibitem[Csardi and Nepusz, 2006]{csardi_nepusz_I2006}
Csardi, G. and Nepusz, T. (2006).
\newblock The igraph software package for complex network research.
\newblock {\em InterJournal}, Complex Systems.

\bibitem[Danaher et~al., 2014]{danaher_etal_JRSSB2014}
Danaher, P., Wang, P., and Witten, D. (2014).
\newblock The joint graphical lasso for inverse covariance estimation accross
  multiple classes.
\newblock {\em Journal of the Royal Statistical Society Series B},
  76(2):373--397.

\bibitem[Fortunato and Barth\'el\'emy, 2007]{fortunato_barthelemy_PNAS2007}
Fortunato, S. and Barth\'el\'emy, M. (2007).
\newblock Resolution limit in community detection.
\newblock In {\em Proceedings of the National Academy of Sciences}, volume 104,
  pages 36--41.
\newblock doi:10.1073/pnas.0605965104; URL:
  \url{http://www.pnas.org/content/104/1/36.abstract}.

\bibitem[Friedman et~al., 2008]{friedman_etal_B2008}
Friedman, J., Hastie, T., and Tibshirani, R. (2008).
\newblock Sparse inverse covariance estimation with the graphical lasso.
\newblock {\em Biostatistics}, 9(3):432--441.

\bibitem[Fruchterman and Reingold, 1991]{fruchterman_reingold_SPE1991}
Fruchterman, T. and Reingold, B. (1991).
\newblock Graph drawing by force-directed placement.
\newblock {\em Software, Practice and Experience}, 21:1129--1164.

\bibitem[Giraud et~al., 2009]{giraud_etal_p2009}
Giraud, C., Huet, S., and Verzelen, N. (2009).
\newblock Graph selection with ggmselect.
\newblock Technical report, preprint arXiv.
\newblock http://fr.arxiv.org/abs/0907.0619.

\bibitem[Laurent and Villa-Vialaneix, 2011]{laurent_villavialaneix_III2011}
Laurent, T. and Villa-Vialaneix, N. (2011).
\newblock Using spatial indexes for labeled network analysis.
\newblock {\em Information, Interaction, Intelligence (I3)}, 11(1).

\bibitem[Meyer et~al., 2008]{meyer_etal_BMCB2008}
Meyer, P., Lafitte, F., and Bontempi, G. (2008).
\newblock minet: A {R/B}ioconductor package for inferring large transcriptional
  networks using mutual information.
\newblock {\em BMC Bioinformatics}, 9(461).

\bibitem[Newman and Girvan, 2004]{newman_girvan_PRE2004}
Newman, M. and Girvan, M. (2004).
\newblock Finding and evaluating community structure in networks.
\newblock {\em Physical Review, E}, 69:026113.

\bibitem[Noack and Rotta, 2009]{noack_rotta_SEA2009}
Noack, A. and Rotta, R. (2009).
\newblock Multi-level algorithms for modularity clustering.
\newblock In {\em SEA 2009: Proceedings of the 8th International Symposium on
  Experimental Algorithms}, pages 257--268, Berlin, Heidelberg.
  Springer-Verlag.

\bibitem[Pearl, 1998]{pearl_RRS1998}
Pearl, J. (1998).
\newblock {\em Probabilistic reasoning in intelligent systems: networks of
  plausible inference}.
\newblock Morgan Kaufmann, San Francisco, California, USA.

\bibitem[Pearl and Russel, 2002]{pearl_russel_HBTNN2002}
Pearl, J. and Russel, S. (2002).
\newblock {\em Bayesian Networks}.
\newblock Bradford Books (MIT Press), Cambridge, Massachussets, USA.

\bibitem[Rossi and Villa-Vialaneix, 2010]{rossi_villavialaneix_N2010}
Rossi, F. and Villa-Vialaneix, N. (2010).
\newblock Optimizing an organized modularity measure for topographic graph
  clustering: a deterministic annealing approach.
\newblock {\em Neurocomputing}, 73(7-9):1142--1163.

\bibitem[Rossi and Villa-Vialaneix, 2011]{rossi_villavialaneix_JSFdS2011}
Rossi, F. and Villa-Vialaneix, N. (2011).
\newblock Repr\'esentation d'un grand r\'eseau \`a partir d'une classification
  hi\'erarchique de ses sommets.
\newblock {\em Journal de la Soci\'et\'e Fran\c{c}aise de Statistique},
  152(3):34--65.

\bibitem[Schaeffer, 2007]{schaeffer_CSR2007}
Schaeffer, S. (2007).
\newblock Graph clustering.
\newblock {\em Computer Science Review}, 1(1):27--64.

\bibitem[Sch\"{a}fer and Strimmer, 2005a]{schafer_strimmer_B2005}
Sch\"{a}fer, J. and Strimmer, K. (2005a).
\newblock An empirical bayes approach to inferring large-scale gene association
  networks.
\newblock {\em Bioinformatics}, 21(6):754--764.

\bibitem[Sch\"{a}fer and Strimmer, 2005b]{schafer_strimmer_SAGMB2005}
Sch\"{a}fer, J. and Strimmer, K. (2005b).
\newblock A shrinkage approach to large-scale covariance matrix estimation and
  implication for functional genomics.
\newblock {\em Statistical Applications in Genetics and Molecular Biology},
  4:1--32.

\bibitem[Scutari, 2010]{scutari_JSS2010}
Scutari, M. (2010).
\newblock Learning {B}ayesian networks with the bnlearn {R} package.
\newblock {\em Journal of Statistical Software}, 35(3):1--22.

\bibitem[Villa-Vialaneix et~al., 2014]{villavialaneix_etal_QTQM2014}
Villa-Vialaneix, N., Vignes, M., Viguerie, N., and San~Cristobal, M. (2014).
\newblock Inferring networks from multiple samples with concensus {LASSO}.
\newblock {\em Quality Technology and Quantitative Management}, 11(1):39--60.

\bibitem[von Luxburg, 2007]{vonluxburg_SC2007}
von Luxburg, U. (2007).
\newblock A tutorial on spectral clustering.
\newblock {\em Statistics and Computing}, 17(4):395--416.

\bibitem[Zanghi et~al., 2008]{zanghi_etal_PR2008}
Zanghi, H., Ambroise, C., and Miele, V. (2008).
\newblock Fast online graph clustering via erd\"{o}s-r\'{e}nyi mixture.
\newblock {\em Pattern Recognition}, 41:3592--3599.

\end{thebibliography}}


\end{document}