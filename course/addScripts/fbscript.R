load("fbnet.rda") 

set.seed(04091748)
fb.net$layout <- layout.fruchterman.reingold(fb.net)
plot(fb.net, vertex.color="springgreen", vertex.frame.color="springgreen",
     vertex.size=3, vertex.label=NA)
dev.print(png, file="my-fb-network-fr-total.png", width=900, height=500)

fb.clusters <- clusters(fb.net)
fb.lcc <- induced.subgraph(fb.net,
                           which(fb.clusters$membership==
                             which.max(fb.clusters$csize)))
set.seed(23081540)
fb.lcc$layout <- layout.fruchterman.reingold(fb.lcc)
plot(fb.lcc, vertex.color="lightyellow", vertex.frame.color="lightyellow",
     vertex.size=3, vertex.label.font=2, vertex.label.color="darkred")

dev.print(png, file="my-fb-network-fr.png", width=700, height=500)
     
set.seed(23081631)
plot(fb.lcc, vertex.color="lightyellow", vertex.frame.color="lightyellow",
     vertex.size=3, vertex.label.font=2, vertex.label.color="darkred",
     layout=layout.random)
     
dev.print(png, file="my-fb-network-raw.png", width=700, height=500)

# global characteristics
graph.density(fb.net)
graph.density(fb.lcc)

transitivity(fb.net)
transitivity(fb.lcc)

# degrees
fb.deg <- degree(fb.lcc)
V(fb.lcc)$hub <- rep("springgreen",vcount(fb.lcc))
V(fb.lcc)$hub[which(fb.deg>=27)] <- rep("yellow",sum(fb.deg>=27))
V(fb.lcc)$hubl <- rep(NA,vcount(fb.lcc))
V(fb.lcc)$hubl[which(fb.deg>=27)] <- V(fb.lcc)$name[which(fb.deg>=27)]
plot(fb.lcc, vertex.color=V(fb.lcc)$hub, vertex.frame.color=V(fb.lcc)$hub,
     vertex.size=3, vertex.label.font=2, vertex.label.color="darkred",
     vertex.label=V(fb.lcc)$hubl)
dev.print(png, file="my-fb-network-fr-hubs.png", width=700, height=500)

plot(sort(unique(fb.deg)),table(fb.deg),pch=19,
     col=c(rep("orange",length(table(fb.deg))-3),rep("darkred",3)),main="",
     xlab="degree",ylab="number of nodes")
dev.print(png, file="my-fb-network-deg-dist.png", width=700, height=500)

# betweenness
fb.bet <- betweenness(fb.lcc)
V(fb.lcc)$hb <- rep("springgreen",vcount(fb.lcc))
V(fb.lcc)$hb[which(fb.bet>=3000)] <- rep("yellow",sum(fb.bet>=3000))
V(fb.lcc)$hbl <- rep(NA,vcount(fb.lcc))
V(fb.lcc)$hbl[which(fb.bet>=3000)] <- V(fb.lcc)$name[which(fb.bet>=3000)]
plot(fb.lcc, vertex.color=V(fb.lcc)$hb, vertex.frame.color=V(fb.lcc)$hb,
     vertex.size=3, vertex.label.font=2, vertex.label.color="darkred",
     vertex.label=V(fb.lcc)$hbl)
dev.print(png, file="my-fb-network-fr-highbet.png", width=700, height=500)

plot(density(fb.bet,bw=50),xlab="betweenness",ylab="density",lwd=2,
     col="darkred",main="")
dev.print(png, file="my-fb-network-bet-dens.png", width=700, height=500)

# communities
fb.clusters <- multilevel.community(fb.lcc)
modularity(fb.clusters)
# plot(fb.clusters,fb.lcc)
# dev.print(png, file="my-fb-network-bet-autocomplot.png", width=700, height=500)
plot(fb.clusters,fb.lcc, vertex.color=fb.clusters$membership,
     vertex.frame.color=fb.clusters$membership, vertex.size=3,
     vertex.label.font=2, vertex.label.color="darkred")
dev.print(png, file="my-fb-network-communities.png",width=700,height=500)
set.seed(1429)
fb.clusters2 <- multilevel.community(fb.lcc)
modularity(fb.clusters2)
plot(fb.clusters2,fb.lcc)