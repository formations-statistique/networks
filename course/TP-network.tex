\documentclass{beamer}

\useoutertheme{infolines}
\usecolortheme{rose} %background of blocks
\usetheme{NathCours}

\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage[english]{babel}
\usepackage{url}
\newcommand{\email}[1]{\href{mailto:#1}{\nolinkurl{#1}}}
\usepackage{multimedia} 
\usepackage{algorithm,algorithmic}
\usepackage{listings}
\lstloadlanguages{R}

\definecolor{vi3}{rgb}{0.5,0,0.5}
\definecolor{jaunepale}{cmyk}{0,0,0.2,0}
\definecolor{tu3}{rgb}{0,0.5,0}
\definecolor{brik}{rgb}{0.8,0.2,0.2}
\definecolor{darkred}{rgb}{0.7,0,0}

\newcommand{\citer}[1]{\textcolor{darkred}{\bf \cite{#1}}}
\newcommand{\gephicommand}[1]{\begin{center}{\footnotesize \textcolor{vi3}{#1}}\end{center}}

\hypersetup{colorlinks=true,
citecolor=vi3, %
pdfmenubar=false,
pdftoolbar=false,
bookmarksopen=true,
breaklinks=true, %permet le retour � la ligne dans les liens trop longs
urlcolor=vi3, %couleur des hyperliens mais la couleur ne change pas !!!
linkcolor=vi3}

% From Philippe Besse for Rcode with listings
\lstset{language=R,basicstyle=\ttfamily,keywordstyle=\color{vi3},commentstyle=\small\color{tu3},backgroundcolor=\color{jaunepale},xleftmargin=2ex,showstringspaces=false}

\def\RR{\textsf{R}\/}
\newcommand{\strong}[1]{{\normalfont\fontseries{b}\selectfont #1}}
\let\pkg=\strong
\RequirePackage{alltt}

\DeclareGraphicsExtensions{.pdf,.jpg,.png}
\graphicspath{{../images/}{images/}}

\begin{document}
\sloppy
\selectlanguage{english}

\logo{\includegraphics[width=2 cm]{inra.jpg}}

\title[Network (TP)]{An introduction to network inference and mining - TP}
\author[Formation INRA]{Nathalie Villa-Vialaneix -
\email{nathalie.villa@toulouse.inra.fr}\\ {\url{http://www.nathalievilla.org}}}
\institute[Niveau 3]{INRA, UR 0875 MIAT}

\date[Nathalie Villa-Vialaneix]{Formation INRA \includegraphics[width=1
cm]{inra.jpg}, Niveau 3}

\begin{frame}[label=slide1]
\titlepage
\end{frame}


\begin{frame}[containsverbatim]
	\frametitle{Packages in \texttt{R}}
	\includegraphics[height=0.5 cm]{R.jpg}\ is provided with basic functions but
more than $3,000$ packages are available on the
\href{{http://cran.r-project.org/}}{CRAN} (Comprehensive R Archive Network) for
additionnal functions (see also the project
\href{{http://www.bioconductor.org/}}{Bioconductor}).

	\begin{itemize}
		\item \valeur{Installing new packages} (has to be done only once) with the
command line or the menu (Windows or Mac OS X or RStudio)
			\begin{lstlisting}
install.packages(c("huge","igraph",
                   "mixOmics"))
			\end{lstlisting}
			\hspace*{-1 cm}\begin{tabular}{cc}
				\includegraphics[height=1.9 cm]{InstallerPaquetWindows.jpg}&
				\includegraphics[height=1.9 cm]{InstallerPaquetMac.jpg}
			\end{tabular}
		\item \valeur{Loading a package} (has to be done each time \RR\ is
re-started)
			\begin{lstlisting}
library(igraph)
			\end{lstlisting}
		\item Be sure you properly set the working directory (directory in which
you put the data files) before you start
	\end{itemize}
\end{frame}

\mode<presentation>{\begin{frame}[label=slide2]
	\frametitle{Outline}
	\tableofcontents
\end{frame}}

\AtBeginSection[]{
\begin{frame}
\frametitle{Outline}
\tableofcontents[current]
\end{frame}}

\mode<article>{\maketitle}

\section{Network inference}

\subsection{Data description}

\begin{frame}[containsverbatim]
	\frametitle{Use case description}
	\begin{block}{Data in the \RR\ package \pkg{mixOmics}}
		\valeur{microarray data}: expression of 120 selected genes potentially
involved in nutritional problems on 40 mice. These data come from a nutrigenomic
study \citer{martin_etal_H2007}.
		\begin{lstlisting}
data(nutrimouse)
summary(nutrimouse)
expr <- nutrimouse$gene
rownames(expr) <- paste(1:nrow(expr),
                        nutrimouse$genotype,
                        nutrimouse$diet,
                        sep="-")
		\end{lstlisting}
	\end{block}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Data distribution}
\begin{lstlisting}
boxplot(expr, names=NULL)
\end{lstlisting}
\begin{center}
\includegraphics[width=0.6\linewidth]{boxplot-nutrimouse}
\end{center}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Gene correlations and clustering}
\begin{lstlisting}
expr.c <- scale(expr)
heatmap(as.matrix(expr.c))
\end{lstlisting}
\begin{center}
\includegraphics[width=0.55\linewidth]{heatmap-nutrimouse}
\end{center}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Hierarchical clustering from raw data}
	\begin{lstlisting}
hclust.tree <- hclust(dist(t(expr)))
plot(hclust.tree)
rect.hclust(hclust.tree, k=7, border=rainbow(7))
\end{lstlisting}
\begin{center}
	\includegraphics[width=0.5\linewidth]{tree-nutrimouse}
\end{center}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Save gene clusters}
	\begin{lstlisting}
hclust.groups <- cutree(hclust.tree, k=7)
table(hclust.groups)
hclust.groups
# 1  2  3  4  5  6  7 
# 18  4 20 52 17  6  3
\end{lstlisting}
\end{frame}

\subsection{Inference with \pkg{glasso}}

\begin{frame}[containsverbatim]
	\frametitle{Sparse linear regression by Maximum Likelihood}
	\valeur{Estimation}: \citer{friedman_etal_B2008} Gaussien framework allows
us to use ML optimization \valeur{with a sparse penalization}
	\[
		\mathcal{L}\left(S|X\right) \textcolor{IXXIdarkred}{+ pen} = \sum_{i=1}^n
\left(\sum_{j=1}^p \log \mathbb{P} (X_i^j|X_i^{-j},\ S_j) \right)
\textcolor{IXXIdarkred}{-\lambda \|S\|_1}
	\]
	
	\begin{lstlisting}
glasso.res <- huge(as.matrix(expr), method="glasso")
glasso.res
# Model: graphical lasso (glasso)
# Input: The Data Matrix
# Path length: 10 
# Graph dimension: 120 
# Sparsity level: 0 -----> 0.2128852 
	\end{lstlisting}
estimates of the concentration matrix $S$ are in
\texttt{glasso.res\$icov[[1]]}, ..., \texttt{glasso.res\$icov[[10]]}, each one
corresponding to a different $\lambda$
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Select $\lambda$ for a targeted density with the StARS method 
\cite{liu_etal_NIPS2010}}
	\begin{lstlisting}
glasso.sel <- huge.select(glasso.res, 
                          criterion="stars")
plot(glasso.sel)
	\end{lstlisting}
	\begin{center}
\includegraphics[width=0.9\linewidth]{glasso-path-sel}
	\end{center}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Using \pkg{igraph} to create the graph}
	From the binary adjacency matrix:
	\begin{lstlisting}
bin.mat <- as.matrix(glasso.sel$opt.icov)!=0
colnames(bin.mat) <- colnames(expr)
\end{lstlisting}
Create an undirected simple graph from the matrix:
\begin{lstlisting}
nutrimouse.net <- simplify(graph.adjacency(bin.mat,
  mode="max"))
nutrimouse.net
# IGRAPH UN-- 120 392 -- 
#   + attr: name (v/c)
	\end{lstlisting}
\end{frame}

\subsection{Basic mining}

\begin{frame}[containsverbatim]
	\frametitle{Connected components}
	The resulting network is not connected:
	\begin{lstlisting}
is.connected(nutrimouse.net)
# [1] FALSE
	\end{lstlisting}
	Connected components are extracted by:
	\begin{lstlisting}
components.nutrimouse <- clusters(nutrimouse.net)
components.nutrimouse
# [1]   1  2  3  2  2  4  5  2  2  2  6  2  7  2
# ...
# 
# $csize
# [1]  7 67  1  6  2  1  1  1  1  1  1  1  1  2
# ...
# 
# $no
# [1] 40
\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Working on a connected subgraph}
	The largest connected component (with 99 nodes) can be extracted with:
	\begin{lstlisting}
nutrimouse.lcc <- induced.subgraph(nutrimouse.net,
  components.nutrimouse$membership==
    which.max(components.nutrimouse$csize))
nutrimouse.lcc
# IGRAPH UN-- 67 375 -- 
#   + attr: name (v/c)
	\end{lstlisting}
	and visualized with:
	\begin{lstlisting}
nutrimouse.lcc$layout <- layout.kamada.kawai(
  nutrimouse.lcc)
plot(nutrimouse.lcc, vertex.size=2,
     vertex.color="lightyellow",
     vertex.frame.color="lightyellow",
     vertex.label.color="black",
     vertex.label.cex=0.7)
	\end{lstlisting}
\end{frame}

\begin{frame}
	\frametitle{Resulting network}
	\begin{center}
		\includegraphics[width=0.6\linewidth]{net-nutrimouse}
	\end{center}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Node clustering (compared to raw clustering)}
	Using one of the clustering function available in \pkg{igraph}:
	\begin{lstlisting}
set.seed(1219)
clusters.nutrimouse <- spinglass.community(
  nutrimouse.lcc)
clusters.nutrimouse
	table(clusters.nutrimouse$membership)
# 1  2  3  4 
# 15 25 17 10
\end{lstlisting}
	The hierarchical clustering for nodes in the largest connected component is
obtained with:
	\begin{lstlisting}
induced.hclust.groups <- hclust.groups[
  components.nutrimouse$membership==
    which.max(components.nutrimouse$csize)]
table(induced.hclust.groups)
# 1  3  4  5  6 
# 6 17 42  1  1
	\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
 \frametitle{Visual comparison}
 \begin{lstlisting}
par(mfrow=c(1,2))
par(mar=rep(1,4))
plot(nutrimouse.lcc, vertex.size=5,
     vertex.color=rainbow(6)[induced.hclust.groups],
     vertex.frame.color=
       rainbow(6)[induced.hclust.groups],
       vertex.label=NA,
     main="Hierarchical clustering")
plot(nutrimouse.lcc, vertex.size=5,
     vertex.color=rainbow(4)[
       clusters.nutrimouse$membership],
     vertex.frame.color=rainbow(4)[
       clusters.nutrimouse$membership],
     vertex.label=NA, main="Graph clustering")
\end{lstlisting}
\end{frame}

\begin{frame}
	\frametitle{Visual comparison}
	\begin{center}
	\includegraphics[width=0.7\linewidth]{comp-clust-nutrimouse}
	\end{center}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Numeric comparison}
	\begin{lstlisting}
compare.communities(induced.hclust.groups,
                    clusters.nutrimouse$membership,
                    method="nmi")
# [1] 0.5091568
modularity(nutrimouse.lcc, induced.hclust.groups)
# [1] 0.2368462
	\end{lstlisting}
\end{frame}


\section{Network mining}

\subsection{Building the graph with \pkg{igraph}}

\begin{frame}[containsverbatim]
	\frametitle{Use case description}
	\begin{block}{Data are Natty's facebook network\footnote{Do it with yours: 
\url{http://shiny.nathalievilla.org/fbs}}}
		\begin{itemize}
			\item \texttt{fbnet-el.txt} is the edge list;
			\item \texttt{fbnet-name.txt} are the nodes' initials.
		\end{itemize}
		\begin{lstlisting}
edgelist <- as.matrix(read.table("fbnet-el.txt"))
vnames <- read.table("fbnet-name.txt")
vnames <- as.character(vnames[,1])
		\end{lstlisting}
	\end{block}
	The graph is built with:
	\begin{lstlisting}
# with `graph.edgelist'
fbnet0 <- graph.edgelist(edgelist, directed=FALSE)
fbnet0
# IGRAPH U--- 152 551 --
	\end{lstlisting}
	See also \valeur{\texttt{help(graph.edgelist)}} for more graph constructors
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Vertexes, vertex attributes}
	The graph's vertexes are accessed and counted with:
	\begin{lstlisting}
V(fbnet0)
vcount(fbnet0)
\end{lstlisting}
	Vertexes can be described by attributes:
\begin{lstlisting}
# add an attribute for vertices
V(fbnet0)$initials <- vnames
fbnet0
# IGRAPH U--- 152 551 -- 
# + attr: initials (v/x)
	\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Edges, edge attributes}
	The graph's edges are accessed and counted with:
	\begin{lstlisting}
E(fbnet0)
# [1] 11 --  1
# [2] 41 --  1
# [3] 52 --  1
# [4] 69 --  1
# [5] 74 --  1
# [6] 75 --  1
# ...
ecount(fbnet0)
# 551
	\end{lstlisting}
	\pkg{igraph} can also handle edge attributes (and also graph attributes).
\end{frame}

\subsection{Global characteristics}

\begin{frame}[containsverbatim]
	\frametitle{Connected components}
	\begin{lstlisting}
is.connected(fbnet0)
# [1] FALSE
\end{Rcode}
As this network is not connected, the connected components can be extracted:
\begin{Rcode}
fb.components <- clusters(fbnet0)
names(fb.components)
# [1] "membership" "csize"      "no"
head(fb.components$membership, 10)
# [1] 1 1 2 2 1 1 1 1 3 1
fb.components$csize
# [1] 122   5   1   1   2   1   1   1   2   1 
# [11]  1   2   1   1   2   3   1   1   1   1
# [21]  1
fb.components$no
# [1] 21
	\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Largest connected component}
	\begin{lstlisting}
fbnet.lcc <- induced.subgraph(fbnet0,
               fb.components$membership==
               which.max(fb.components$csize))
# main characteristics of the LCC
fbnet.lcc
# IGRAPH U--- 122 535 -- 
# + attr: initials (v/x)
is.connected(fbnet.lcc)
# [1] TRUE
	\end{lstlisting}
	and global characteristics
	\begin{lstlisting}
graph.density(fbnet.lcc)
# [1] 0.0724834
transitivity(fbnet.lcc)
# [1] 0.5604524
	\end{lstlisting}
\end{frame}

\subsection{Visualization}

\begin{frame}[containsverbatim]
	\frametitle{Network visualization}
	Different layouts are implemented in \pkg{igraph} to visualize the graph:
	\begin{lstlisting}
plot(fbnet.lcc, layout=layout.random,
     main="random layout", vertex.size=3,
     vertex.color="pink", vertex.frame.color="pink",
     vertex.label.color="darkred",
     edge.color="grey",
     vertex.label=V(fbnet.lcc)$initials) 
	\end{lstlisting}
Try also \texttt{layout.circle}, \texttt{layout.kamada.kawai},
\texttt{layout.fruchterman.reingold}... Network are generated with some
randomness. See also \valeur{\texttt{help(igraph.plotting)}} for more
information on network visualization

	\pkg{igraph} integrates a pre-defined graph attribute \texttt{layout}:
	\begin{lstlisting}
V(fbnet.lcc)$label <- V(fbnet.lcc)$initials
plot(fbnet.lcc)
	\end{lstlisting}
\end{frame}

\begin{frame}
	\begin{center}
	\includegraphics[width=0.7\linewidth]{all-fb-layouts.png}
	\end{center}
\end{frame}

\subsection{Individual characteristics}

\begin{frame}[containsverbatim]
	\frametitle{Degree and betweenness}
	\begin{lstlisting}
fbnet.degrees <- degree(fbnet.lcc)
summary(fbnet.degrees)
#    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
#    1.00    2.00    6.00    8.77   15.00   31.00
fbnet.between <- betweenness(fbnet.lcc)
summary(fbnet.between)
#    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
#    0.00    0.00   14.03  301.70  123.10 3439.00
	\end{lstlisting}
	and their distributions:
	\begin{lstlisting}
par(mfrow=c(1,2))
plot(density(fbnet.degrees), lwd=2,
     main="Degree distribution", xlab="Degree",
     ylab="Density")
plot(density(fbnet.between), lwd=2,
     main="Betweenness distribution",
     xlab="Betweenness", ylab="Density")
	\end{lstlisting}
\end{frame}

\begin{frame}
	\frametitle{Degree and betweenness distribution}
	\begin{center}
		\includegraphics[width=0.8\linewidth]{degbet-distrib}
	\end{center}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Combine visualization and individual characteristics}
\begin{lstlisting}
par(mar=rep(1,4))
# set node attribute `size' with degree
V(fbnet.lcc)$size <- 2*sqrt(fbnet.degrees)
# set node attribute `color' with betweenness
bet.col <- cut(log(fbnet.between+1),10,
               labels=FALSE)
V(fbnet.lcc)$color <- heat.colors(10)[11-bet.col]
plot(fbnet.lcc, main="Degree and betweenness",
     vertex.frame.color=heat.colors(10)[bet.col],
     vertex.label=NA, edge.color="grey")
\end{lstlisting}
\end{frame}

\begin{frame}
	\begin{center}
		\includegraphics[width=0.7\linewidth]{degbet-net}
	\end{center}
\end{frame}

\subsection{Clustering}

\begin{frame}[containsverbatim]
	\frametitle{Node clustering}
	One of the function to perform node clustering is
\texttt{spinglass.community} (that prossibly produces different results each
time it is used since it is based on a stochastic process):
	\begin{lstlisting}
fbnet.clusters <- spinglass.community(fbnet.lcc)
fbnet.clusters
# Graph community structure calculated with
#   the spinglass algorithm
# Number of communities: 9 
# Modularity: 0.5654136 
# Membership vector:
#   [1] 9 5 6 5 5 9 1 9 1 1 4 9 9 6 2 7 2 7 2 7 5
#  ...
table(fbnet.clusters$membership)
#  1  2  3  4  5  6  7  8  9 
#  8  7  8 32 14  7 18  2 26
\end{lstlisting}
	See \valeur{\texttt{help(communities)}} for more information.
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Combine clustering and visualization}
	\begin{lstlisting}
# create a new attribute
V(fbnet.lcc)$community <- fbnet.clusters$membership
fbnet.lcc
# IGRAPH U--- 122 535 -- 
#   + attr: layout (g/n), initials (v/c),
# label (v/c), size (v/n), color (v/c),
# community (v/n)
\end{lstlisting}

Display the clustering:
\begin{lstlisting}
par(mfrow=c(1,1))
par(mar=rep(1,4))
plot(fbnet.lcc, main="Communities", 
     vertex.frame.color=
       rainbow(9)[fbnet.clusters$membership],
     vertex.color=
       rainbow(9)[fbnet.clusters$membership],
     vertex.label=NA, edge.color="grey")
\end{lstlisting}
\end{frame}

\begin{frame}
	\frametitle{Combine clustering and visualization}
	\begin{center}
	\includegraphics[width=0.65\linewidth]{communities}
	\end{center}
\end{frame}

\begin{frame}[containsverbatim]
	\frametitle{Export the graph}
	The \texttt{graphml} format can be used to export the graph (it can be read by
most of the graph visualization programs and includes information on node and
edge attributes)
\begin{lstlisting}
write.graph(fbnet.lcc, file="fblcc.graphml",
            format="graphml")
\end{lstlisting}
see \valeur{\texttt{help(write.graph)}} for more information of graph
exportation formats
\end{frame}

\section{Use gephi}

\begin{frame}
	\frametitle{Import data}
	Start \includegraphics[width=0.5 cm]{images/gephi.jpg}\ and select ``new
project''.
	\begin{itemize}
		\item \valeur{Create a graph from an edge list}: file
\texttt{fbnet-el.txt}\\
		\gephicommand{File / Open\\Graph type: ``undirected''}
		\item<2-> \valeur{Create a graph (with nodes an edges attributes) from a
GraphML file}: file \texttt{fblcc.graphml} previously created with igraph\\
		\gephicommand{File / Open\\Graph type: ``undirected''}
		On the right part of the screen, the number of nodes and edges are
indicated.
		\item<3-> \valeur{Check nodes attributes} and define nodes labels
		\gephicommand{Data lab\\
		Copy data to a column: initials\\
		Copy to: Label}
		Other nodes or edges attributes can be imported from a csv file with
\gephicommand{import file}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Visualization}
	\begin{itemize}
		\item \valeur{Visualize the graph with Fruchterman \& Reingold algorithm}
		\gephicommand{Bottom left of panel ``Global view''\\
		Spatialization: Choose ``Fruchterman and Reingold''\\
		Area: 800 - Gravity: 1\\
		Click on ``Stop'' when stabilized}
		\item<2-> \valeur{Customize the view}:
		\begin{itemize}
			\item zoom or de-zoom with the mouse
			\item change link width (bottom toolbox)
			\item display labels and change labels size and color (bottom toolbox)
			\item with the ``select'' tool, visualize a node neighbors (top left
toolbox)
			\item with the ``move'' tool, move a node (top left toolbox)
		\end{itemize}
		\item<3-> \valeur{Use the view to understand the graph} (delete labels
before)
		\begin{itemize}
			\item Color a node's neighbors (middle left toolbox)
			\item Visualize the shortest path between two nodes (middle left toolbox)
			\item Visualize the distances from a given node to all the other nodes by
nodes coloring (middle left toolbox)
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Graph mining}
	\begin{itemize}
		\item \valeur{Node characteristics}
		\gephicommand{Window / Statistics\\
		On the bottom right panel, Degree: run\\
		Check on Data Lab\\
		On the top left panel, Clustering / Nodes / Size: Choose a clustering
parameter: Degree\\
		Size: Min size:10, Max size: 50, Apply\\
		On the bottom right panel, Shortest path: run\\
		Color: Choose a clustering parameter: Betweenness centrality\\
		Default: choose a palette, Apply}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Node clustering}
	\begin{itemize}
		\item \valeur{Node characteristics}
		\gephicommand{On the bottom right panel, Modularity: run\\
		Check on Data Lab\\
		On the top left panel, Clustering / Nodes / Label color: Choose a clustering
parameter: Modularity class\\
		Color: Default: choose a palette, Apply}
		\item<2-> \valeur{Export a view}
		\gephicommand{Preview / Default with straight links / Refresh / Export
(bottom left)}
	\end{itemize}
\end{frame}

% {\tiny \bibliographystyle{apalike}
% \bibliography{/home/nathalie/Private/Travail/Modeles/LaTeX/bibliototal}}

{\tiny
\begin{thebibliography}{}

\bibitem[Friedman et~al., 2008]{friedman_etal_B2008}
Friedman, J., Hastie, T., and Tibshirani, R. (2008).
\newblock Sparse inverse covariance estimation with the graphical lasso.
\newblock {\em Biostatistics}, 9(3):432--441.

\bibitem[Liu et~al., 2010]{liu_etal_NIPS2010}
Liu, H., Roeber, K., and Wasserman, L. (2010).
\newblock Stability approach to regularization selection for high dimensional
  graphical models.
\newblock In {\em Proceedings of Neural Information Processing Systems (NIPS
  2010)}, pages 1432--1440, Vancouver, Canada.

\bibitem[Martin et~al., 2007]{martin_etal_H2007}
Martin, P., Guillou, H., Lasserre, F., D\'ejean, S., Lan, A., Pascussi, J.,
  San~Cristobal, M., Legrand, P., Besse, P., and Pineau, T. (2007).
\newblock Novel aspects of \protect{PPAR}$\alpha$-mediated regulation of lipid
  and xenobiotic metabolism revealed through a multrigenomic study.
\newblock {\em Hepatology}, 54:767--777.

\end{thebibliography}}


\end{document}