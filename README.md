# Introduction à l'analyse et l'inférence de réseaux / Introduction to network inference and mining

Ce dépôt contient le source des supports de cours de la formation <a href="http://www.nathalievialaneix.eu/teaching/network.html" target="_blank">Introduction à l'analyse et l'inférence de réseaux</a>.

This repository contains source code for accompanying material of the training session <a href="http://www.nathalievialaneix.eu/teaching/network.html" target="_blank">Introduction to network inference and mining</a>. Material is in English.


<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
